@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
            @if(Session::get('success'))
                    <div class="alert alert-success">
                        {{session::get('success')}}
                    </div>
            @endif
                <div class="panel-heading">
                    My Service Lists

                    <a class="pull-right" href="{{ route('add-service') }}">
                        Add Service
                    </a>
                </div>

                <div class="panel-body">

                @if(!empty($services))
                    
                <table class="table">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Price</th>
                        <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    @foreach($services as $key => $service)
                        <tr>
                            <th scope="row">{{ $key+1 }}</th>
                            <td>{{ $service['name'] }}</td>
                            <td>{{ $service['price'] }}</td>
                            <td>update | delete</td>
                        </tr>
                    @endforeach
                        
                    </tbody>
                    </table>

                    @else
                        No Record Found!
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
