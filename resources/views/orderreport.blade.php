@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
            @if(Session::get('success'))
                    <div class="alert alert-success">
                        {{session::get('success')}}
                    </div>
            @endif
                <div class="panel-heading">
                    Order Report
                </div>

                <form action="order-report" method="get">
                <label for="from">From</label>
                <input type="text" id="from" name="from" autocomplete="off">
                <label for="to">to</label>
                <input type="text" id="to" name="to" autocomplete="off">
                <input type="submit" value="search">
                </form>

                <div class="panel-body">

                <?php
                $gmap_array = array();
                ?>
                @if(!empty($orders))
                    
                <table class="table">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Customer Name</th>
                        <th scope="col">Customer Phone Number</th>
                        <th scope="col">Customer Service</th>
                        <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    @foreach($orders as $key => $orders)
                        <tr>
                            <th scope="row">{{ $key+1 }}</th>
                            <td>{{ $orders->cname }}</td>
                            <td>{{ $orders->cphone }}</td>
                            <td>{{ $orders->sname }}</td>
                            <td>update | delete</td>
                        </tr>
                        <?php
                        array_push($gmap_array,"['Customer Name : $orders->cname <br> Customer Number : $orders->cphone <br> Customer Service : $orders->sname', $orders->lat, $orders->lng, $key]");
                        ?>
                    @endforeach

                    <?php
                    $gmap_array = implode(',',$gmap_array);
                    ?>
                    </tbody>
                    </table>

                    @else
                        No Record Found!
                    @endif
                </div>
                <div id="map" style="width: 500px; height: 400px;"></div>

            </div>
        </div>
    </div>
</div>
@endsection

@section('js')

<script type="text/javascript">
   
    function initMap() {
        
    var locations = [
        {!! $gmap_array !!}
    ];
    
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 10,
      center: new google.maps.LatLng(locations[0][1], locations[0][2]),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    
    var infowindow = new google.maps.InfoWindow();

    var marker, i;
    
    for (i = 0; i < locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });
      
      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
}
  </script>
@endsection