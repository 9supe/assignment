@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
            @if(Session::get('success'))
                    <div class="alert alert-success">
                        {{session::get('success')}}
                    </div>
            @endif
                <div class="panel-heading">
                    My Order Service Lists

                    <a class="pull-right" href="{{ route('book-a-service') }}">
                        Book A Service
                    </a>
                </div>

                <div class="panel-body">

                @if(!empty($orders))
                    
                <table class="table">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Service Name</th>
                        <th scope="col">Service Person Name</th>
                        <th scope="col">Service Person Phone</th>
                        <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    @foreach($orders as $key => $orders)
                        <tr>
                            <th scope="row">{{ $key+1 }}</th>
                            <td>{{ $orders->sname }}</td>
                            <td>{{ $orders->spname }}</td>
                            <td>{{ $orders->sphone }}</td>
                            <td>update | delete</td>
                        </tr>
                    @endforeach
                        
                    </tbody>
                    </table>

                    @else
                        No Record Found!
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
