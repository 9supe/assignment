<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Auth;

class Order extends Model
{
    protected $table = 'orders';

    public static function getOrders($q) {
        $result = DB::table('orders')
         ->selectRaw('orders.id as oid, users.name as cname,users.phone as cphone, services.name as sname, orders.lat as lat, orders.lng as lng')
         ->leftJoin('users', 'orders.customer_id', '=', 'users.id')
         ->leftJoin('services', 'orders.service_id', '=', 'services.id');

        if(!empty($q['fromdate']) && !empty($q['todate'])){ echo 'asdasdas';
            $fromdate = date("Y-m-d H:i:s", strtotime($q['fromdate'])); 
            $todate = date("Y-m-d H:i:s", strtotime($q['todate'])); 
            $result->whereBetween('booking_date', [$fromdate, $todate]);
        }
        $result->where('orders.service_provider_id', Auth::user()->id);
        return $result->get();
    }
}
