<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Order;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function getMyOrderServices(){
        $orders = DB::table('orders')
        ->selectRaw('orders.id as oid, users.name as spname, services.name as sname, users.phone as sphone')
        ->leftJoin('users', 'orders.service_provider_id', '=', 'users.id')
        ->leftJoin('services', 'orders.service_id', '=', 'services.id')
        ->where('orders.customer_id', Auth::user()->id)
        ->get();

        $param = array(
            'orders' => $orders
        );
        return view('myorderservices',$param);
    }

    public function getBookaservice(){
        $orders = Order::all();
        $param = array(
            'orders' => $orders
        );
        return view('bookservice',$param);
    }

    public function postBookservice(Request $request){
        
        $lat = $_POST['lat'];
        $lon = $_POST['lon'];

        $data = DB::table("services")
            ->select("*"
                ,DB::raw("3959 * acos(cos(radians(" . $lat . ")) 
                * cos(radians(lat)) 
                * cos(radians(lng) - radians(" . $lon . ")) 
                + sin(radians(" .$lat. ")) 
                * sin(radians(lat))) AS distance"))
                ->orderBy('distance')
                ->limit(1)
                ->get();
 
      $oder_data = array(
          'booking_date' => date('Y-m-d H:i:s'),
          'customer_id' => Auth::user()->id,
          'service_id' => $data[0]->id,
          'service_provider_id' => $data[0]->user_id,
          'lat' => $lat,
          'lng' => $lon,
      );
      Order::insert($oder_data);

      return redirect('my-order-services')->withSuccess('Your service added successfully.');

    }
}
