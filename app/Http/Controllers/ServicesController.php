<?php

namespace App\Http\Controllers;

use App\Service;
use App\Order;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;

class ServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::where('user_id',Auth::user()->id)->get();
        $param = array(
            'services' => $services
        );
        return view('myservices',$param);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('addservice');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $lat = $_POST['lat'];
        $lon = $_POST['lon'];

        $data = array(
            'name' => $_POST['name'],
            'user_id' => Auth::user()->id,
            'price' => $_POST['price'],
            'service_type' => $_POST['service_type'],
            'lat' => $lat,
            'lng' => $lon,
        );
        Service::insert($data);
        return redirect('my-services')->withSuccess('Your service added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        $fromdate = $todate = '';
        if(isset($_GET['from']) && isset($_GET['to'])){
            $fromdate = $_GET['from'];
            $todate = $_GET['to'];
        }
        $q = array(
            'fromdate' => $fromdate,
            'todate' => $todate
        );
        $orders = Order::getOrders($q);
        
        $param = array(
            'orders' => $orders
        );
        return view('orderreport',$param);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Service $service)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
        //
    }

}
