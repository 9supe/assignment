<?php

use Illuminate\Support\Facades\Route;
  
use App\Http\Controllers\RegisterController;
Route::auth(); 
Route::group(['middleware' => 'guest'], function(){
    //Auth routes for non-authenticated users
    Route::get('/', function () {
        return view('login');
    });
});


// Route::get('logout',  'RegisterController@getLogout');
// Route::post('registration',  'RegisterController@postRegistration');
// Route::post('login',  'RegisterController@postLogin');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/', function () {
        //return 'Hello! You are logged in.';
        return view('home');
    });

    // customer
    Route::get('/book-a-service', 'HomeController@getBookaservice')->name('book-a-service');
    Route::get('/my-order-services', 'HomeController@getMyOrderServices')->name('my-order-services');
    Route::post('/book-service', 'HomeController@postBookservice');

    // supplier
    Route::get('/order-report', 'ServicesController@show')->name('order-report');
    Route::get('/my-services', 'ServicesController@index')->name('my-services');
    Route::get('/add-service', 'ServicesController@create')->name('add-service');
    Route::post('/add-service', 'ServicesController@store');
});

Route::auth();